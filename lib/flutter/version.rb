# frozen_string_literal: true

# :nocov:

module Flutter
  VERSION = "0.2.3"
end
